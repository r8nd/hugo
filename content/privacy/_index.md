---
title: "Privacy Policy"
date: 2021-01-01T08:47:11+01:00
draft: false
---

<div id="background" class="container text-center">
<img src="./imgs/fingerprint.svg" alt="" style="width:200px; opacity:0.1" />
</div>

Unlike so many policies you will come across on the internet these days, mine is pretty simple.

> 
> I do not store your data, period.
> 

No cookies. No ads. Nada.

You won't leave fingerprints here, no matter how hard you try. 

---

In fact, at the moment there is no way for you to get in contact with me if you wanted to. I am hoping to fix that in the near future. But for now, all you will find here is __content__. 
