---
title: "About R8ND"
date: 2021-01-01T08:47:11+01:00
draft: false
---

This site is an outlet for my many hobbies, one being web development. It is a public sandbox for me to experiment out in the open and have some fun outside of work. Given my various interests, there may be a lot of _random_ bits and pieces that accumulate here. Capturing it here forces me to analyze and refine my thoughts, pushing me to work a little harder and push a little further as I try new things.

You are welcome to look around. I hope you enjoy your stay!

# TODO: Check out this font for logo ideas: https://www.myfonts.com/fonts/roman-melikhov/conneqt?tab=glyphs&utm_medium=email&utm_campaign=BestOf2020_Day4_01142021&utm_content=BestOf2020_Day4_01142021+CID_aa7bb0ebf9c3b541ae5aa7d5429626b4&utm_source=newsletter&utm_term=Conneqt
