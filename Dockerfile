FROM alpine:3.12.3

RUN apk update \
	&& apk --no-cache add bash hugo vim git openssh

ARG USERNAME=r8nd
ARG USER_UID=888
ARG USER_GID=$USER_UID

# Create and set the user
RUN addgroup --gid $USER_GID --system $USERNAME \
    && adduser --uid $USER_UID --ingroup $USERNAME --system --disabled-password --no-create-home --home /workspace --shell /bin/sh $USERNAME
### --shell /usr/sbin/nologin # to prevent being able to execute a shell (and logging into remotely without additional parameters)
USER $USERNAME

# Expose any ports you need 
#EXPOSE 

#COPY . .

#CMD ["/bin/sh"]
